package id.co.bvk.repository;

import id.co.bvk.domain.PurchaseItem;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the PurchaseItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurchaseItemRepository extends R2dbcRepository<PurchaseItem, Long>, PurchaseItemRepositoryInternal {
    @Query("SELECT * FROM purchase_item entity WHERE entity.purchase_id = :id")
    Flux<PurchaseItem> findByPurchase(Long id);

    @Query("SELECT * FROM purchase_item entity WHERE entity.purchase_id IS NULL")
    Flux<PurchaseItem> findAllWherePurchaseIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<PurchaseItem> findAll();

    @Override
    Mono<PurchaseItem> findById(Long id);

    @Override
    <S extends PurchaseItem> Mono<S> save(S entity);
}

interface PurchaseItemRepositoryInternal {
    <S extends PurchaseItem> Mono<S> insert(S entity);
    <S extends PurchaseItem> Mono<S> save(S entity);
    Mono<Integer> update(PurchaseItem entity);

    Flux<PurchaseItem> findAll();
    Mono<PurchaseItem> findById(Long id);
    Flux<PurchaseItem> findAllBy(Pageable pageable);
    Flux<PurchaseItem> findAllBy(Pageable pageable, Criteria criteria);
}
