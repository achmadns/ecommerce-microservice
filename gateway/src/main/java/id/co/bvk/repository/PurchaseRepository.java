package id.co.bvk.repository;

import id.co.bvk.domain.Purchase;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Purchase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurchaseRepository extends R2dbcRepository<Purchase, Long>, PurchaseRepositoryInternal {
    @Query("SELECT * FROM purchase entity WHERE entity.user_id = :id")
    Flux<Purchase> findByUser(Long id);

    @Query("SELECT * FROM purchase entity WHERE entity.user_id IS NULL")
    Flux<Purchase> findAllWhereUserIsNull();

    // just to avoid having unambigous methods
    @Override
    Flux<Purchase> findAll();

    @Override
    Mono<Purchase> findById(Long id);

    @Override
    <S extends Purchase> Mono<S> save(S entity);
}

interface PurchaseRepositoryInternal {
    <S extends Purchase> Mono<S> insert(S entity);
    <S extends Purchase> Mono<S> save(S entity);
    Mono<Integer> update(Purchase entity);

    Flux<Purchase> findAll();
    Mono<Purchase> findById(Long id);
    Flux<Purchase> findAllBy(Pageable pageable);
    Flux<Purchase> findAllBy(Pageable pageable, Criteria criteria);
}
