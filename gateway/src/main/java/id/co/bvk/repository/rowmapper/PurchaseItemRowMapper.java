package id.co.bvk.repository.rowmapper;

import id.co.bvk.domain.PurchaseItem;
import id.co.bvk.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link PurchaseItem}, with proper type conversions.
 */
@Service
public class PurchaseItemRowMapper implements BiFunction<Row, String, PurchaseItem> {

    private final ColumnConverter converter;

    public PurchaseItemRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link PurchaseItem} stored in the database.
     */
    @Override
    public PurchaseItem apply(Row row, String prefix) {
        PurchaseItem entity = new PurchaseItem();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setProductId(converter.fromRow(row, prefix + "_product_id", Long.class));
        entity.setPurchaseId(converter.fromRow(row, prefix + "_purchase_id", Long.class));
        return entity;
    }
}
