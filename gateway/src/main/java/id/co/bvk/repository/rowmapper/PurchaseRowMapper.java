package id.co.bvk.repository.rowmapper;

import id.co.bvk.domain.Purchase;
import id.co.bvk.service.ColumnConverter;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Purchase}, with proper type conversions.
 */
@Service
public class PurchaseRowMapper implements BiFunction<Row, String, Purchase> {

    private final ColumnConverter converter;

    public PurchaseRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Purchase} stored in the database.
     */
    @Override
    public Purchase apply(Row row, String prefix) {
        Purchase entity = new Purchase();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setAmount(converter.fromRow(row, prefix + "_amount", Double.class));
        entity.setStatus(converter.fromRow(row, prefix + "_status", String.class));
        entity.setUserId(converter.fromRow(row, prefix + "_user_id", String.class));
        return entity;
    }
}
