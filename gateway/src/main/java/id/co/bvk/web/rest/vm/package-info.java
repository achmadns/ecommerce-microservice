/**
 * View Models used by Spring MVC REST controllers.
 */
package id.co.bvk.web.rest.vm;
