import { Component, Vue, Inject } from 'vue-property-decorator';

import { IPurchaseItem } from '@/shared/model/purchase-item.model';
import PurchaseItemService from './purchase-item.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class PurchaseItemDetails extends Vue {
  @Inject('purchaseItemService') private purchaseItemService: () => PurchaseItemService;
  @Inject('alertService') private alertService: () => AlertService;

  public purchaseItem: IPurchaseItem = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.purchaseItemId) {
        vm.retrievePurchaseItem(to.params.purchaseItemId);
      }
    });
  }

  public retrievePurchaseItem(purchaseItemId) {
    this.purchaseItemService()
      .find(purchaseItemId)
      .then(res => {
        this.purchaseItem = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
