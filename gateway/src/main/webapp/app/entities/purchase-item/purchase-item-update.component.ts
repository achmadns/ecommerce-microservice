import { Component, Vue, Inject } from 'vue-property-decorator';

import AlertService from '@/shared/alert/alert.service';

import PurchaseService from '@/entities/purchase/purchase.service';
import { IPurchase } from '@/shared/model/purchase.model';

import { IPurchaseItem, PurchaseItem } from '@/shared/model/purchase-item.model';
import PurchaseItemService from './purchase-item.service';

const validations: any = {
  purchaseItem: {
    productId: {},
  },
};

@Component({
  validations,
})
export default class PurchaseItemUpdate extends Vue {
  @Inject('purchaseItemService') private purchaseItemService: () => PurchaseItemService;
  @Inject('alertService') private alertService: () => AlertService;

  public purchaseItem: IPurchaseItem = new PurchaseItem();

  @Inject('purchaseService') private purchaseService: () => PurchaseService;

  public purchases: IPurchase[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.purchaseItemId) {
        vm.retrievePurchaseItem(to.params.purchaseItemId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.purchaseItem.id) {
      this.purchaseItemService()
        .update(this.purchaseItem)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('gatewayApp.purchaseItem.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.purchaseItemService()
        .create(this.purchaseItem)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('gatewayApp.purchaseItem.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrievePurchaseItem(purchaseItemId): void {
    this.purchaseItemService()
      .find(purchaseItemId)
      .then(res => {
        this.purchaseItem = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.purchaseService()
      .retrieve()
      .then(res => {
        this.purchases = res.data;
      });
  }
}
