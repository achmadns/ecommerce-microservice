import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IPurchaseItem } from '@/shared/model/purchase-item.model';

import PurchaseItemService from './purchase-item.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class PurchaseItem extends Vue {
  @Inject('purchaseItemService') private purchaseItemService: () => PurchaseItemService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public purchaseItems: IPurchaseItem[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllPurchaseItems();
  }

  public clear(): void {
    this.retrieveAllPurchaseItems();
  }

  public retrieveAllPurchaseItems(): void {
    this.isFetching = true;
    this.purchaseItemService()
      .retrieve()
      .then(
        res => {
          this.purchaseItems = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IPurchaseItem): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removePurchaseItem(): void {
    this.purchaseItemService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('gatewayApp.purchaseItem.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllPurchaseItems();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
