import { Component, Vue, Inject } from 'vue-property-decorator';

import { decimal, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import PurchaseItemService from '@/entities/purchase-item/purchase-item.service';
import { IPurchaseItem } from '@/shared/model/purchase-item.model';

import UserOAuth2Service from '@/entities/user/user.oauth2.service';

import { IPurchase, Purchase } from '@/shared/model/purchase.model';
import PurchaseService from './purchase.service';

const validations: any = {
  purchase: {
    amount: {
      required,
      decimal,
    },
    status: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class PurchaseUpdate extends Vue {
  @Inject('purchaseService') private purchaseService: () => PurchaseService;
  @Inject('alertService') private alertService: () => AlertService;

  public purchase: IPurchase = new Purchase();

  @Inject('purchaseItemService') private purchaseItemService: () => PurchaseItemService;

  public purchaseItems: IPurchaseItem[] = [];

  @Inject('userOAuth2Service') private userOAuth2Service: () => UserOAuth2Service;

  public users: Array<any> = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.purchaseId) {
        vm.retrievePurchase(to.params.purchaseId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.purchase.id) {
      this.purchaseService()
        .update(this.purchase)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('gatewayApp.purchase.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.purchaseService()
        .create(this.purchase)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('gatewayApp.purchase.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrievePurchase(purchaseId): void {
    this.purchaseService()
      .find(purchaseId)
      .then(res => {
        this.purchase = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.purchaseItemService()
      .retrieve()
      .then(res => {
        this.purchaseItems = res.data;
      });
    this.userOAuth2Service()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
  }
}
