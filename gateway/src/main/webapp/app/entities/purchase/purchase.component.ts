import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IPurchase } from '@/shared/model/purchase.model';

import PurchaseService from './purchase.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Purchase extends Vue {
  @Inject('purchaseService') private purchaseService: () => PurchaseService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public purchases: IPurchase[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllPurchases();
  }

  public clear(): void {
    this.retrieveAllPurchases();
  }

  public retrieveAllPurchases(): void {
    this.isFetching = true;
    this.purchaseService()
      .retrieve()
      .then(
        res => {
          this.purchases = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IPurchase): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removePurchase(): void {
    this.purchaseService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('gatewayApp.purchase.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllPurchases();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
