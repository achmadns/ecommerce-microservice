import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const PurchaseItem = () => import('@/entities/purchase-item/purchase-item.vue');
// prettier-ignore
const PurchaseItemUpdate = () => import('@/entities/purchase-item/purchase-item-update.vue');
// prettier-ignore
const PurchaseItemDetails = () => import('@/entities/purchase-item/purchase-item-details.vue');
// prettier-ignore
const Purchase = () => import('@/entities/purchase/purchase.vue');
// prettier-ignore
const PurchaseUpdate = () => import('@/entities/purchase/purchase-update.vue');
// prettier-ignore
const PurchaseDetails = () => import('@/entities/purchase/purchase-details.vue');
// prettier-ignore
const Product = () => import('@/entities/product/product.vue');
// prettier-ignore
const ProductUpdate = () => import('@/entities/product/product-update.vue');
// prettier-ignore
const ProductDetails = () => import('@/entities/product/product-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/purchase-item',
    name: 'PurchaseItem',
    component: PurchaseItem,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase-item/new',
    name: 'PurchaseItemCreate',
    component: PurchaseItemUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase-item/:purchaseItemId/edit',
    name: 'PurchaseItemEdit',
    component: PurchaseItemUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase-item/:purchaseItemId/view',
    name: 'PurchaseItemView',
    component: PurchaseItemDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase',
    name: 'Purchase',
    component: Purchase,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase/new',
    name: 'PurchaseCreate',
    component: PurchaseUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase/:purchaseId/edit',
    name: 'PurchaseEdit',
    component: PurchaseUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/purchase/:purchaseId/view',
    name: 'PurchaseView',
    component: PurchaseDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/product',
    name: 'Product',
    component: Product,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/product/new',
    name: 'ProductCreate',
    component: ProductUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/product/:productId/edit',
    name: 'ProductEdit',
    component: ProductUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/product/:productId/view',
    name: 'ProductView',
    component: ProductDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
