export interface IProduct {
  id?: number;
  name?: string | null;
  description?: string | null;
  stock?: number | null;
  price?: number | null;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string | null,
    public description?: string | null,
    public stock?: number | null,
    public price?: number | null
  ) {}
}
