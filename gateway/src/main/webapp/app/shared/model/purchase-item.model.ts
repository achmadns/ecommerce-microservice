import { IPurchase } from '@/shared/model/purchase.model';

export interface IPurchaseItem {
  id?: number;
  productId?: number | null;
  purchase?: IPurchase | null;
}

export class PurchaseItem implements IPurchaseItem {
  constructor(public id?: number, public productId?: number | null, public purchase?: IPurchase | null) {}
}
