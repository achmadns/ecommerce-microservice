import { IPurchaseItem } from '@/shared/model/purchase-item.model';
import { IUser } from '@/shared/model/user.model';

export interface IPurchase {
  id?: number;
  amount?: number;
  status?: string;
  purchaseItems?: IPurchaseItem[] | null;
  user?: IUser | null;
}

export class Purchase implements IPurchase {
  constructor(
    public id?: number,
    public amount?: number,
    public status?: string,
    public purchaseItems?: IPurchaseItem[] | null,
    public user?: IUser | null
  ) {}
}
