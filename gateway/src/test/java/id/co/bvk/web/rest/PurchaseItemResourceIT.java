package id.co.bvk.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import id.co.bvk.IntegrationTest;
import id.co.bvk.domain.PurchaseItem;
import id.co.bvk.repository.PurchaseItemRepository;
import id.co.bvk.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PurchaseItemResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PurchaseItemResourceIT {

    private static final Long DEFAULT_PRODUCT_ID = 1L;
    private static final Long UPDATED_PRODUCT_ID = 2L;

    private static final String ENTITY_API_URL = "/api/purchase-items";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PurchaseItemRepository purchaseItemRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private PurchaseItem purchaseItem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchaseItem createEntity(EntityManager em) {
        PurchaseItem purchaseItem = new PurchaseItem().productId(DEFAULT_PRODUCT_ID);
        return purchaseItem;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchaseItem createUpdatedEntity(EntityManager em) {
        PurchaseItem purchaseItem = new PurchaseItem().productId(UPDATED_PRODUCT_ID);
        return purchaseItem;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(PurchaseItem.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        purchaseItem = createEntity(em);
    }

    @Test
    void createPurchaseItem() throws Exception {
        int databaseSizeBeforeCreate = purchaseItemRepository.findAll().collectList().block().size();
        // Create the PurchaseItem
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeCreate + 1);
        PurchaseItem testPurchaseItem = purchaseItemList.get(purchaseItemList.size() - 1);
        assertThat(testPurchaseItem.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    void createPurchaseItemWithExistingId() throws Exception {
        // Create the PurchaseItem with an existing ID
        purchaseItem.setId(1L);

        int databaseSizeBeforeCreate = purchaseItemRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllPurchaseItemsAsStream() {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        List<PurchaseItem> purchaseItemList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(PurchaseItem.class)
            .getResponseBody()
            .filter(purchaseItem::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(purchaseItemList).isNotNull();
        assertThat(purchaseItemList).hasSize(1);
        PurchaseItem testPurchaseItem = purchaseItemList.get(0);
        assertThat(testPurchaseItem.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    void getAllPurchaseItems() {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        // Get all the purchaseItemList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(purchaseItem.getId().intValue()))
            .jsonPath("$.[*].productId")
            .value(hasItem(DEFAULT_PRODUCT_ID.intValue()));
    }

    @Test
    void getPurchaseItem() {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        // Get the purchaseItem
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, purchaseItem.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(purchaseItem.getId().intValue()))
            .jsonPath("$.productId")
            .value(is(DEFAULT_PRODUCT_ID.intValue()));
    }

    @Test
    void getNonExistingPurchaseItem() {
        // Get the purchaseItem
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPurchaseItem() throws Exception {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();

        // Update the purchaseItem
        PurchaseItem updatedPurchaseItem = purchaseItemRepository.findById(purchaseItem.getId()).block();
        updatedPurchaseItem.productId(UPDATED_PRODUCT_ID);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPurchaseItem.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPurchaseItem))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
        PurchaseItem testPurchaseItem = purchaseItemList.get(purchaseItemList.size() - 1);
        assertThat(testPurchaseItem.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    void putNonExistingPurchaseItem() throws Exception {
        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();
        purchaseItem.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, purchaseItem.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPurchaseItem() throws Exception {
        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();
        purchaseItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPurchaseItem() throws Exception {
        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();
        purchaseItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePurchaseItemWithPatch() throws Exception {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();

        // Update the purchaseItem using partial update
        PurchaseItem partialUpdatedPurchaseItem = new PurchaseItem();
        partialUpdatedPurchaseItem.setId(purchaseItem.getId());

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPurchaseItem.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchaseItem))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
        PurchaseItem testPurchaseItem = purchaseItemList.get(purchaseItemList.size() - 1);
        assertThat(testPurchaseItem.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    void fullUpdatePurchaseItemWithPatch() throws Exception {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();

        // Update the purchaseItem using partial update
        PurchaseItem partialUpdatedPurchaseItem = new PurchaseItem();
        partialUpdatedPurchaseItem.setId(purchaseItem.getId());

        partialUpdatedPurchaseItem.productId(UPDATED_PRODUCT_ID);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPurchaseItem.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchaseItem))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
        PurchaseItem testPurchaseItem = purchaseItemList.get(purchaseItemList.size() - 1);
        assertThat(testPurchaseItem.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    void patchNonExistingPurchaseItem() throws Exception {
        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();
        purchaseItem.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, purchaseItem.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPurchaseItem() throws Exception {
        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();
        purchaseItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPurchaseItem() throws Exception {
        int databaseSizeBeforeUpdate = purchaseItemRepository.findAll().collectList().block().size();
        purchaseItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchaseItem))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the PurchaseItem in the database
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePurchaseItem() {
        // Initialize the database
        purchaseItemRepository.save(purchaseItem).block();

        int databaseSizeBeforeDelete = purchaseItemRepository.findAll().collectList().block().size();

        // Delete the purchaseItem
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, purchaseItem.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<PurchaseItem> purchaseItemList = purchaseItemRepository.findAll().collectList().block();
        assertThat(purchaseItemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
