package id.co.bvk.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

import id.co.bvk.IntegrationTest;
import id.co.bvk.domain.Purchase;
import id.co.bvk.repository.PurchaseRepository;
import id.co.bvk.service.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link PurchaseResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class PurchaseResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/purchases";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Purchase purchase;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Purchase createEntity(EntityManager em) {
        Purchase purchase = new Purchase().amount(DEFAULT_AMOUNT).status(DEFAULT_STATUS);
        return purchase;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Purchase createUpdatedEntity(EntityManager em) {
        Purchase purchase = new Purchase().amount(UPDATED_AMOUNT).status(UPDATED_STATUS);
        return purchase;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Purchase.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        purchase = createEntity(em);
    }

    @Test
    void createPurchase() throws Exception {
        int databaseSizeBeforeCreate = purchaseRepository.findAll().collectList().block().size();
        // Create the Purchase
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeCreate + 1);
        Purchase testPurchase = purchaseList.get(purchaseList.size() - 1);
        assertThat(testPurchase.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPurchase.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createPurchaseWithExistingId() throws Exception {
        // Create the Purchase with an existing ID
        purchase.setId(1L);

        int databaseSizeBeforeCreate = purchaseRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchaseRepository.findAll().collectList().block().size();
        // set the field null
        purchase.setAmount(null);

        // Create the Purchase, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchaseRepository.findAll().collectList().block().size();
        // set the field null
        purchase.setStatus(null);

        // Create the Purchase, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllPurchasesAsStream() {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        List<Purchase> purchaseList = webTestClient
            .get()
            .uri(ENTITY_API_URL)
            .accept(MediaType.APPLICATION_NDJSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentTypeCompatibleWith(MediaType.APPLICATION_NDJSON)
            .returnResult(Purchase.class)
            .getResponseBody()
            .filter(purchase::equals)
            .collectList()
            .block(Duration.ofSeconds(5));

        assertThat(purchaseList).isNotNull();
        assertThat(purchaseList).hasSize(1);
        Purchase testPurchase = purchaseList.get(0);
        assertThat(testPurchase.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPurchase.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void getAllPurchases() {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        // Get all the purchaseList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(purchase.getId().intValue()))
            .jsonPath("$.[*].amount")
            .value(hasItem(DEFAULT_AMOUNT.doubleValue()))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS));
    }

    @Test
    void getPurchase() {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        // Get the purchase
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, purchase.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(purchase.getId().intValue()))
            .jsonPath("$.amount")
            .value(is(DEFAULT_AMOUNT.doubleValue()))
            .jsonPath("$.status")
            .value(is(DEFAULT_STATUS));
    }

    @Test
    void getNonExistingPurchase() {
        // Get the purchase
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewPurchase() throws Exception {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();

        // Update the purchase
        Purchase updatedPurchase = purchaseRepository.findById(purchase.getId()).block();
        updatedPurchase.amount(UPDATED_AMOUNT).status(UPDATED_STATUS);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedPurchase.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedPurchase))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
        Purchase testPurchase = purchaseList.get(purchaseList.size() - 1);
        assertThat(testPurchase.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPurchase.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();
        purchase.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, purchase.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();
        purchase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();
        purchase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdatePurchaseWithPatch() throws Exception {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();

        // Update the purchase using partial update
        Purchase partialUpdatedPurchase = new Purchase();
        partialUpdatedPurchase.setId(purchase.getId());

        partialUpdatedPurchase.amount(UPDATED_AMOUNT);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPurchase.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchase))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
        Purchase testPurchase = purchaseList.get(purchaseList.size() - 1);
        assertThat(testPurchase.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPurchase.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void fullUpdatePurchaseWithPatch() throws Exception {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();

        // Update the purchase using partial update
        Purchase partialUpdatedPurchase = new Purchase();
        partialUpdatedPurchase.setId(purchase.getId());

        partialUpdatedPurchase.amount(UPDATED_AMOUNT).status(UPDATED_STATUS);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedPurchase.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchase))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
        Purchase testPurchase = purchaseList.get(purchaseList.size() - 1);
        assertThat(testPurchase.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPurchase.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();
        purchase.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, purchase.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();
        purchase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().collectList().block().size();
        purchase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(purchase))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deletePurchase() {
        // Initialize the database
        purchaseRepository.save(purchase).block();

        int databaseSizeBeforeDelete = purchaseRepository.findAll().collectList().block().size();

        // Delete the purchase
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, purchase.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Purchase> purchaseList = purchaseRepository.findAll().collectList().block();
        assertThat(purchaseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
