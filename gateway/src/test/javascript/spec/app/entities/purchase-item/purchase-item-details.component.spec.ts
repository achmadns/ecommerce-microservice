/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import PurchaseItemDetailComponent from '@/entities/purchase-item/purchase-item-details.vue';
import PurchaseItemClass from '@/entities/purchase-item/purchase-item-details.component';
import PurchaseItemService from '@/entities/purchase-item/purchase-item.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('PurchaseItem Management Detail Component', () => {
    let wrapper: Wrapper<PurchaseItemClass>;
    let comp: PurchaseItemClass;
    let purchaseItemServiceStub: SinonStubbedInstance<PurchaseItemService>;

    beforeEach(() => {
      purchaseItemServiceStub = sinon.createStubInstance<PurchaseItemService>(PurchaseItemService);

      wrapper = shallowMount<PurchaseItemClass>(PurchaseItemDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { purchaseItemService: () => purchaseItemServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundPurchaseItem = { id: 123 };
        purchaseItemServiceStub.find.resolves(foundPurchaseItem);

        // WHEN
        comp.retrievePurchaseItem(123);
        await comp.$nextTick();

        // THEN
        expect(comp.purchaseItem).toBe(foundPurchaseItem);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPurchaseItem = { id: 123 };
        purchaseItemServiceStub.find.resolves(foundPurchaseItem);

        // WHEN
        comp.beforeRouteEnter({ params: { purchaseItemId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.purchaseItem).toBe(foundPurchaseItem);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
