/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import PurchaseItemUpdateComponent from '@/entities/purchase-item/purchase-item-update.vue';
import PurchaseItemClass from '@/entities/purchase-item/purchase-item-update.component';
import PurchaseItemService from '@/entities/purchase-item/purchase-item.service';

import PurchaseService from '@/entities/purchase/purchase.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('PurchaseItem Management Update Component', () => {
    let wrapper: Wrapper<PurchaseItemClass>;
    let comp: PurchaseItemClass;
    let purchaseItemServiceStub: SinonStubbedInstance<PurchaseItemService>;

    beforeEach(() => {
      purchaseItemServiceStub = sinon.createStubInstance<PurchaseItemService>(PurchaseItemService);

      wrapper = shallowMount<PurchaseItemClass>(PurchaseItemUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          purchaseItemService: () => purchaseItemServiceStub,
          alertService: () => new AlertService(),

          purchaseService: () => new PurchaseService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.purchaseItem = entity;
        purchaseItemServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(purchaseItemServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.purchaseItem = entity;
        purchaseItemServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(purchaseItemServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundPurchaseItem = { id: 123 };
        purchaseItemServiceStub.find.resolves(foundPurchaseItem);
        purchaseItemServiceStub.retrieve.resolves([foundPurchaseItem]);

        // WHEN
        comp.beforeRouteEnter({ params: { purchaseItemId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.purchaseItem).toBe(foundPurchaseItem);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
