/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import PurchaseItemComponent from '@/entities/purchase-item/purchase-item.vue';
import PurchaseItemClass from '@/entities/purchase-item/purchase-item.component';
import PurchaseItemService from '@/entities/purchase-item/purchase-item.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('PurchaseItem Management Component', () => {
    let wrapper: Wrapper<PurchaseItemClass>;
    let comp: PurchaseItemClass;
    let purchaseItemServiceStub: SinonStubbedInstance<PurchaseItemService>;

    beforeEach(() => {
      purchaseItemServiceStub = sinon.createStubInstance<PurchaseItemService>(PurchaseItemService);
      purchaseItemServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<PurchaseItemClass>(PurchaseItemComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          purchaseItemService: () => purchaseItemServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      purchaseItemServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllPurchaseItems();
      await comp.$nextTick();

      // THEN
      expect(purchaseItemServiceStub.retrieve.called).toBeTruthy();
      expect(comp.purchaseItems[0]).toEqual(expect.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      purchaseItemServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removePurchaseItem();
      await comp.$nextTick();

      // THEN
      expect(purchaseItemServiceStub.delete.called).toBeTruthy();
      expect(purchaseItemServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});
