package com.myapp.repository;

import com.myapp.domain.Purchase;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Purchase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
    @Query("select purchase from Purchase purchase where purchase.user.login = ?#{principal.preferredUsername}")
    List<Purchase> findByUserIsCurrentUser();
}
