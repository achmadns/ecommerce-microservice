package com.myapp.web.rest;

import com.myapp.domain.PurchaseItem;
import com.myapp.repository.PurchaseItemRepository;
import com.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.myapp.domain.PurchaseItem}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class PurchaseItemResource {

    private final Logger log = LoggerFactory.getLogger(PurchaseItemResource.class);

    private static final String ENTITY_NAME = "userPurchaseItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PurchaseItemRepository purchaseItemRepository;

    public PurchaseItemResource(PurchaseItemRepository purchaseItemRepository) {
        this.purchaseItemRepository = purchaseItemRepository;
    }

    /**
     * {@code POST  /purchase-items} : Create a new purchaseItem.
     *
     * @param purchaseItem the purchaseItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new purchaseItem, or with status {@code 400 (Bad Request)} if the purchaseItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/purchase-items")
    public ResponseEntity<PurchaseItem> createPurchaseItem(@RequestBody PurchaseItem purchaseItem) throws URISyntaxException {
        log.debug("REST request to save PurchaseItem : {}", purchaseItem);
        if (purchaseItem.getId() != null) {
            throw new BadRequestAlertException("A new purchaseItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PurchaseItem result = purchaseItemRepository.save(purchaseItem);
        return ResponseEntity
            .created(new URI("/api/purchase-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /purchase-items/:id} : Updates an existing purchaseItem.
     *
     * @param id the id of the purchaseItem to save.
     * @param purchaseItem the purchaseItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated purchaseItem,
     * or with status {@code 400 (Bad Request)} if the purchaseItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the purchaseItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/purchase-items/{id}")
    public ResponseEntity<PurchaseItem> updatePurchaseItem(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PurchaseItem purchaseItem
    ) throws URISyntaxException {
        log.debug("REST request to update PurchaseItem : {}, {}", id, purchaseItem);
        if (purchaseItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purchaseItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purchaseItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PurchaseItem result = purchaseItemRepository.save(purchaseItem);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, purchaseItem.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /purchase-items/:id} : Partial updates given fields of an existing purchaseItem, field will ignore if it is null
     *
     * @param id the id of the purchaseItem to save.
     * @param purchaseItem the purchaseItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated purchaseItem,
     * or with status {@code 400 (Bad Request)} if the purchaseItem is not valid,
     * or with status {@code 404 (Not Found)} if the purchaseItem is not found,
     * or with status {@code 500 (Internal Server Error)} if the purchaseItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/purchase-items/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PurchaseItem> partialUpdatePurchaseItem(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PurchaseItem purchaseItem
    ) throws URISyntaxException {
        log.debug("REST request to partial update PurchaseItem partially : {}, {}", id, purchaseItem);
        if (purchaseItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purchaseItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purchaseItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PurchaseItem> result = purchaseItemRepository
            .findById(purchaseItem.getId())
            .map(existingPurchaseItem -> {
                if (purchaseItem.getProductId() != null) {
                    existingPurchaseItem.setProductId(purchaseItem.getProductId());
                }

                return existingPurchaseItem;
            })
            .map(purchaseItemRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, purchaseItem.getId().toString())
        );
    }

    /**
     * {@code GET  /purchase-items} : get all the purchaseItems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of purchaseItems in body.
     */
    @GetMapping("/purchase-items")
    public List<PurchaseItem> getAllPurchaseItems() {
        log.debug("REST request to get all PurchaseItems");
        return purchaseItemRepository.findAll();
    }

    /**
     * {@code GET  /purchase-items/:id} : get the "id" purchaseItem.
     *
     * @param id the id of the purchaseItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the purchaseItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/purchase-items/{id}")
    public ResponseEntity<PurchaseItem> getPurchaseItem(@PathVariable Long id) {
        log.debug("REST request to get PurchaseItem : {}", id);
        Optional<PurchaseItem> purchaseItem = purchaseItemRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(purchaseItem);
    }

    /**
     * {@code DELETE  /purchase-items/:id} : delete the "id" purchaseItem.
     *
     * @param id the id of the purchaseItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/purchase-items/{id}")
    public ResponseEntity<Void> deletePurchaseItem(@PathVariable Long id) {
        log.debug("REST request to delete PurchaseItem : {}", id);
        purchaseItemRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
